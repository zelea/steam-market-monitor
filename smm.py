#!/data/data/com.termux/files/usr/bin/python
from time import sleep
from typing import Any
import requests
import os
from enum import Enum
import numpy as np
import math

class _CURRENCY:
    sign: str
    _format: str
    value: int
    
    def __int__(self) -> int:
        return self.value
    
    def __str__(self) -> str:
        return self._format
    
    def __format__(self, spec: Any) -> str:
        return str(self) % format(spec, ",.2f").replace(',', ' ')

class _EUR(_CURRENCY):
    sign = '€'
    _format = f"%s{sign}"
    value = 3

class Currency(Enum):
    EUR= _EUR()  # The euro €

class Price:
    def __init__(self, lowest_price: float, volume: int, median_price: float, currency: Currency = Currency.EUR) -> None:
        self.lowest_price: float = lowest_price
        self.volume: int = volume
        self.median_price: float = median_price
        self.currency: _CURRENCY = currency.value
    
    def __str__(self) -> str:
        return f"{self.format_price(self.lowest_price)} {['■', '▲', '▼'][int(np.sign(self.difference))]} {'+' if self.difference > 0 else ''}{self.format_price(self.difference)} | {format(self.volume, ',').replace(',', ' ')} pcs"
    
    def format_price(self, price: float) -> str:
        return self.currency.__format__(price)
    
    @property
    def difference(self) -> float:
        return round(self.lowest_price - self.median_price, 2)
    
    @classmethod
    def from_dict(cls, d: dict) -> 'Price':
        if d['success']:
            return cls(
                lowest_price= float(d['lowest_price'].replace(',', '.').replace(Currency.EUR.value.sign, '').replace('-', '0').replace(' ', '')), 
                volume= int(d['volume'].replace(',', '')), 
                median_price= float(d['median_price'].replace(',', '.').replace(Currency.EUR.value.sign, '').replace('-', '0').replace(' ', ''))
                )
        else:
            raise ValueError()

    @staticmethod
    def price_with_fees( price: float ) -> float:
        if price > 0:
            steam_fee = math.floor( max( price * 5, 1 ) ) / 100
            publisher_fee = math.floor( max( price * 10, 1 ) ) / 100

            return round(price + steam_fee + publisher_fee, 2)

    @staticmethod
    def price_without_fees(price: float) -> float:
        if price > 0:
            price = price * 100
            oprice = price
            price = int(price / 1.15)
            while oprice != price_with_fees(price)*100:
                print(price)
                if oprice > price_with_fees(price)*100:
                    price -= 1
                else:
                    price += 1
            return round(price/100, 2)

class Item:
    def __init__(self, name: str, currency: Currency = Currency.EUR) -> None:
        self.name: str = name
        self.currency: _CURRENCY = currency.value

    @property
    def price(self) -> 'Price':
        r = requests.get(f"https://steamcommunity.com/market/priceoverview/?appid=730&currency={int(self.currency)}&market_hash_name={requests.utils.quote(self.name)}")
        return Price.from_dict(r.json())
    
    def __str__(self) -> str:
        return f"{self.name}: \n{self.price}"

def main():
    items = [Item(item) for item in [
            "Operation Broken Fang Case", 
            "Operation Riptide Case", 
            "Snakebite Case", 
            "Dreams & Nightmares Case"
        ]]
    
    while True:
        try:
            c ='\n'.join(map(str, items))
        except Exception as e:
            os.system(f'termux-notification --icon trending_up --id 1338 --alert-once --ongoing -c "{e}"')
            print(e)
        else:
            command = f'termux-notification --icon trending_up --title "Steam market" --priority max --group 1339 --id 1338 --alert-once --ongoing -c "{c}" --action "am start --user 0 -n com.valvesoftware.android.steam.community/.activity.MainActivity"'
            print(command)
            os.system(command)
        sleep(300)

if __name__ == "__main__":
    main()