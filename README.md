**Download:**

Termux

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    height="80">](https://f-droid.org/packages/com.termux)

Termux:API

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    height="80">](https://f-droid.org/packages/com.termux.api)

Termux:Boot (optional)

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    height="80">](https://f-droid.org/packages/com.termux.boot)

**Also download** (inside Termux):

Termux:API's packages:
`pkg install termux-api`

Python:
`pkg install python`

NumPy:
`pip install numpy` or `pkg install python-numpy`

Python requests library:
`pip install requests`


Add executable rights for the file:
`chmod +x smm.py`

If you want to run the script on every startup, put it into the `~/.termux/boot/` directory.
